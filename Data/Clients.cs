using System;
using System.Text.Json.Serialization;

namespace TodoList.Data
{
    public class Client
    {
        [JsonPropertyName("displayName")]
        public string DisplayName { get; set; }
        [JsonPropertyName("mail")]
        public string Mail { get; set; }
        [JsonPropertyName("id")]
        public string Id { get; set; }
    }
}