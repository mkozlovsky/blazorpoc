using System.Collections.Generic;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace TodoList.Data
{
    class ClientsResponse{
        [JsonPropertyName("value")]
        public List<Client> Value {get; set;}
    }

    public class ClientsService
    {
        private HttpClient client;
        private ILogger<ClientsService> _logger;
        public ClientsService(HttpClient client, ILogger<ClientsService> logger)
        {
           this.client = client; 
           _logger = logger;
        }

        public async Task<List<Client>> ListClients() 
        {
            var reqTask = client.GetStreamAsync("http://localhost:8000/b2b/users");
            var data = await JsonSerializer.DeserializeAsync<ClientsResponse>(await reqTask);
            return data.Value;
        }
    }
}